import * as React from 'react';
import { Route, Switch } from 'react-router';
import { BrowserRouter } from 'react-router-dom';

import { testRouter } from './routes/testRouter';
import { TestComponent } from './components';

export const App = () => {
  return (
    <BrowserRouter>
      <Switch>
        {testRouter.map(props => {
          return <Route key={props.path} path={props.path} component={props.component} exact={props.exact} />;
        })}
        <Route key={'someKey'} component={TestComponent} exact={true} path={'/test2'} />
      </Switch>
    </BrowserRouter>
  );
};
