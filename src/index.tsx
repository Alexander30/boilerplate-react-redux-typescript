import * as React from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import * as ReactDOM from 'react-dom';

import { store }         from './store';
import { App }           from './app';

ReactDOM.render(
  <Provider store={store}>
      <App/>
  </Provider>,
  document.getElementById('app')
);

module.hot.accept();
