import * as React from 'react';

export interface RouteElement {
  path: string;
  exact: boolean;
  component: React.ComponentType;
}
