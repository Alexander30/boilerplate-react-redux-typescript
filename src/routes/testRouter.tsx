import * as React from 'react';
import { RouteElement } from './types';

import { TestComponent, AnotherTestComponent } from '../components';

export const testRouter: RouteElement[] = [
  {
    path: '/',
    exact: true,
    component: AnotherTestComponent
  },
  {
    path: '/test',
    exact: true,
    component: TestComponent
  }
];
