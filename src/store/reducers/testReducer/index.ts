interface State {
  testReducer: string;
}

interface Action {
  type: string;
}

const initialState = {
  testReducer: 'hi, i am test data'
};

export const testReducer = (state: State = initialState, action: Action) => {
  switch (action.type) {
    default:
      return state;
  }
};
