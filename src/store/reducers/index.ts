import { combineReducers } from 'redux';
import { testReducer } from './testReducer';

export const baseReducer = combineReducers({ testReducer });
