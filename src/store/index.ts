import { composeWithDevTools } from 'redux-devtools-extension';
import { applyMiddleware, createStore } from 'redux';
import { baseReducer } from './reducers';

import thunk from 'redux-thunk';

export const store = createStore(baseReducer, composeWithDevTools(applyMiddleware(thunk)));
